import json
import socket
import sys
from bot import ExtremeG


class MessageHandler(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.bot = ExtremeG(self)

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def join_specific(self, track):
        return self.msg("joinRace", {"botId": {"name": self.name,
                                 "key": self.key}, "trackName":track, "carCount":1})

    def switch_lane(self, direction):
        self.msg('switchLane', direction)

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        #self.join()
        self.join_specific("usa")
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.throttle(1)
        #self.ping()

    def on_game_init(self, data):
        data = data['race']
        self.bot.track = data['track']

    def on_game_start(self, data):
        print("Race started")
        self.throttle(1)

    def on_car_positions(self, data):
        self.bot.run(data)

    def on_crash(self, data):
        self.bot.on_crash(data)

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_your_car(self, data):
        self.bot.color = data['color']

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_your_car,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                #try:
                    msg_map[msg_type](data)
                #except:
                #    pass
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = MessageHandler(s, name, key)
        bot.run()
