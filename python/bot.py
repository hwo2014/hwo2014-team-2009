import math, sys, random

class ExtremeG(object):
    CENTRIPETA = 0.576
    ACELERACAO = 0.1
    CRASH_FACTOR = 1

    def __init__(self, msg):
        self.msg = msg
        self.prevAngle = 0
        self.prevDeltaAngle = 0
        self.prevLocation = 0
        self.speed = self.prevSpeed = 0
        self.last_piece = 0
        self.lane = self.wanted_lane = 0
        self.is_switching = self.has_switched = False
        self.crashed_on_last_piece = False
        self.max_angle = 0

        self.t1 = self.t2 = -1
        self.desired_speeds = {}

    def nullGetter(self): return None

    def setTrack(self, track):
        self.pieces = track['pieces']
        self.lanes = track['lanes']
        self.starting_positions = []

        self.totalLength = 0
        for x in self.pieces:
            self.starting_positions.append(self.totalLength)
            self.totalLength += self.piece_length(x)


        self.maxLane = max(lane['index'] for lane in track['lanes'])

    track = property(nullGetter, setTrack)

    def piece_radius(self, piece):
        if piece['angle'] >= 0:
            radius = float(piece['radius'] - self.lanes[self.lane]['distanceFromCenter'])
        else:
            radius = float(piece['radius'] + self.lanes[self.lane]['distanceFromCenter'])
        return radius

    def piece_length(self, piece):
        if 'length' in piece: return piece['length']
        radius = self.piece_radius(piece)
        return radius * abs(math.radians(piece['angle']))

    def on_crash(self, data):
        if data['color'] == self.color:
            #self.CRASH_FACTOR *= 0.8
            self.CENTRIPETA *= 0.8
            self.ACELERACAO *= 0.8

    def desired_speed(self, piece_index):
        if (piece_index, self.lane) not in self.desired_speeds:
            radius = self.piece_radius(self.pieces[piece_index])
            self.desired_speeds[(piece_index, self.lane)] = (self.CENTRIPETA * radius) ** 0.5
        return self.desired_speeds[(piece_index, self.lane)] * self.CRASH_FACTOR

    def needs_to_stop(self, distance, speed, desired_speed):
        if self.t2 == -1: return False

        speedmult = (self.ds2-self.ds1)/(self.s2-self.s1)
        ct = self.ds1 - speedmult*self.s1

        #print (ct, speedmult)

        distance -= speed
        speed = speed + ct + speedmult * speed

        while distance > 0 and speed > desired_speed:
            speed += speedmult*speed
            distance -= speed

        return speed > desired_speed
        #delta_sq = speed**2 - desired_speed**2
        #return delta_sq / (2*self.ACELERACAO) > distance+5

    def get_throttle(self, angle, delta_angle, delta_delta_angle, speed, radius):

        distance_to_bend = self.piece_length(self.pieces[self.piece_index]) - self.my_position['inPieceDistance']

        i = (self.piece_index+1) % len(self.pieces)
        while i != self.piece_index:
            if 'radius' in self.pieces[i]:
                desired_speed = self.desired_speed(i)
                #if i==20: print i, distance_to_bend, speed, desired_speed
                if self.needs_to_stop(distance_to_bend, speed, desired_speed):
                    self.log('0c, throttle 0', False)
                    return 0

                radius = self.piece_radius(self.pieces[i])

            distance_to_bend += self.piece_length(self.pieces[i])

            i = (i+1) % len(self.pieces)

        if 'radius' not in self.pieces[self.piece_index]:
            self.log('full speed in the straightaway!', False)
            return 1

        if delta_angle*self.pieces[self.piece_index]['angle'] < 0:
            self.log('fc, throttle 1', False)
            return 1

        if abs(delta_delta_angle) < 0.3 and abs(delta_angle) < 1.5 and abs(angle) < 40:
            self.log('sc, throttle 0.75', False)
            return 0.75

        self.log('tc condition: (%f * 9 + %f + %f) / %f' % (delta_angle, angle, (speed**10-6**10) / 130000000., radius), False)
        if (abs(delta_angle) > 2 or abs(delta_delta_angle) > 0.8 or (abs(delta_angle) * 9 + abs(angle) + (speed**10-6**10) / 130000000.) / radius > 0.6):
            self.log('tc, throttle 0.1', False)
            return 0.1

        return 0.75

    @staticmethod
    def log(msg, stdout):
        if stdout: print(msg)
        sys.stderr.write(msg+'\n')

    def get_distance(self, other):
        return

    def run(self, data):
        #try:
            my_data = [x for x in data if x['id']['color'] == self.color][0]

            my_position = self.my_position = my_data['piecePosition']

            piece_index = self.piece_index = my_position['pieceIndex']
            if piece_index != self.last_piece:
                if (self.last_piece, self.lane) in self.desired_speeds:
                    if self.crashed_on_last_piece:
                        self.desired_speeds[(self.last_piece, self.lane)] = min(self.prevSpeed, self.desired_speeds[(self.last_piece, self.lane)]) * 0.9
                    elif self.max_angle < 45 and self.piece_speed > self.desired_speeds[(self.last_piece, self.lane)] - 0.1:
                        self.desired_speeds[(self.last_piece, self.lane)] *= 1.1
                    elif self.max_angle > 55:
                        self.desired_speeds[(self.last_piece, self.lane)] = min(self.prevSpeed, self.desired_speeds[(self.last_piece, self.lane)])

                self.crashed_on_last_piece = False
                self.max_angle = 0
                self.piece_speed = self.speed

            self.lane = end_lane = my_position['lane']['endLaneIndex']
            start_lane = my_position['lane']['startLaneIndex']

            if self.lane == self.wanted_lane:
                self.is_switching = False

            if 'switch' in self.pieces[piece_index] and not self.is_switching:
                self.has_switched = False

            #print self.lane, self.maxLane, self.is_switching
            #TODO switch lanes if car in front
            #for other in data:
            #    if other['id']['color'] == self.color: continue
            #    other_position = other['piecePosition']
            #    distance = self.get_distance(other_position)

            if not self.is_switching and not self.has_switched:
                if random.random() > 0.5:
                    if end_lane != self.maxLane:
                        self.is_switching = True
                        self.wanted_lane = end_lane + 1
                        self.msg.switch_lane('Right')
                        return
                    elif end_lane != 0:
                        self.is_switching = True
                        self.wanted_lane = end_lane - 1
                        self.msg.switch_lane('Left')
                else:
                    self.has_switched = True

            #TODO turbo if last lap

            angle = my_data['angle']
            delta_angle = angle - self.prevAngle
            delta_delta_angle = delta_angle - self.prevDeltaAngle

            radius_factor = 1
            radius = 1e18
            angle_sgn = 1
            if 'radius' in self.pieces[piece_index]:
                if self.pieces[piece_index]['angle'] >= 0:
                    radius = float(self.pieces[piece_index]['radius'] - self.lanes[end_lane]['distanceFromCenter'])
                else:
                    radius = float(self.pieces[piece_index]['radius'] + self.lanes[end_lane]['distanceFromCenter'])
                    angle_sgn = -1

                radius_factor = (self.pieces[piece_index]['radius']) / radius

            location = self.starting_positions[my_position['pieceIndex']] + my_position['inPieceDistance']

            if piece_index == self.last_piece:
                self.speed = location - self.prevLocation
                if self.speed < -self.totalLength/2.0: self.speed += self.totalLength
            elif 'radius' in self.pieces[self.piece_index]:
                self.log("Radius of new piece is %f, would want to be at %f" % (radius, self.desired_speed(piece_index)), True)

            delta_speed = self.speed - self.prevSpeed
            if delta_speed and self.prevSpeed:
                if self.t1 == -1:
                    self.t1 = 1
                    self.ds1 = delta_speed
                    self.s1 = self.prevSpeed
                elif self.t2 == -1:
                    self.t2 = 1
                    self.ds2 = delta_speed
                    self.s2 = self.prevSpeed

            self.log('piece: %d loc = %.2f v=%lf angle=%lf' % (piece_index, location, self.speed, angle), True)
            acc = self.get_throttle(angle, delta_angle, delta_delta_angle, self.speed, radius)
            #self.msg.throttle(acc)
            self.msg.throttle(1)#0.64)

            if abs(delta_speed) < 1:
                self.log('ST A %.15f DA %.15f DDA %.15f V %.15f R %.15f AS %d' % ( angle, delta_angle, delta_delta_angle, self.speed, radius, angle_sgn), False)

            self.prevLocation = location
            self.prevAngle = angle
            self.prevDeltaAngle = delta_angle
            self.prevSpeed = self.speed
            self.last_piece = piece_index
            self.max_angle = max(self.max_angle, abs(angle))

        #except:
        #    self.msg.throttle(0.5)