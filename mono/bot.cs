using System;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Turbo
{
    public int ticks;
    public double factor;

    public Turbo(int ticks, double factor)
    {
        this.ticks = ticks;
        this.factor = factor;
    }
}

public class State
{
    public double angle;
    public double delta_angle;
    public double speed;
    public int piece_index;
    public double inPieceDistance;
    public int lane;
    public double throttle;
    public int action;
    public Turbo turbo;
    public Turbo active;

    public State(double a = 0, double da = 0, double v = 0, int p = 0, double ipd = 0, int la = 0, double th = 0)
    {
        angle = a;
        delta_angle = da;
        speed = v;
        piece_index = p;
        inPieceDistance = ipd;
        lane = la;
        throttle = th;
    }

    public State clone()
    {
        State ot = (State)this.MemberwiseClone();
        if (active != null) ot.active = new Turbo(active.ticks, active.factor);
        return ot;
    }

    public bool is_bend()
    {
        return ExtremeG.pieces[piece_index]["radius"] != null;
    }

    public double radius
    {
        get
        {
            return ExtremeG.piece_radius((JObject)ExtremeG.pieces[piece_index], lane);
        }
    }

    public double turn_angle
    {
        get
        {
            return (double)ExtremeG.pieces[piece_index]["angle"];
        }
    }

    public void debug()
    {
        Console.Error.WriteLine("DEBUG piece: " + piece_index + " " +
                   "loc = " + inPieceDistance + " " +
                   "v=" + speed + " " +
                   "angle=" + angle);
    }

    public State advance(double throttle)
    {
        double delta_delta_angle = Model.get_dda(this);
        delta_angle += delta_delta_angle;
        angle += delta_angle;

        if (active != null)
        {
            if (--active.ticks == 0) active = null;
            else this.throttle *= active.factor;
        }
        speed = Model.get_speed(this);

        double rem = speed;
        while (true)
        {
            double length = ExtremeG.piece_length((JObject)ExtremeG.pieces[piece_index], lane);
            if (inPieceDistance + rem > length)
            {
                piece_index = (piece_index + 1) % ((JArray)ExtremeG.pieces).Count;
                rem -= (length - inPieceDistance);
                inPieceDistance = 0;
            }
            else
            {
                inPieceDistance += rem;
                break;
            }
        }

        this.throttle = throttle;

        return this;
    }

    public double distance()
    {
        return inPieceDistance / ExtremeG.piece_length((JObject)ExtremeG.pieces[piece_index], lane) * 100;
    }

    internal bool switch_decision()
    {
        int next_piece = (piece_index + 1) % ((JArray)ExtremeG.pieces).Count;
        double length = ExtremeG.piece_length((JObject)ExtremeG.pieces[piece_index], lane) - inPieceDistance;
        return speed < length && speed * 2 > length && (bool?)ExtremeG.pieces[next_piece]["switch"] == true;
    }
}

public class StateComparer : IComparer<State>
{
    public double h(State a)
    {
        //double std = a.inPieceDistance * ExtremeG.piece_length(ExtremeG.pieces[a.piece_index], a.lane) / ExtremeG.piece_length(ExtremeG.pieces[a.piece_index], 0);
        return -(a.piece_index + a.inPieceDistance / 100 + a.speed);
    }
    public int Compare(State a, State b)
    {
        double ff = h(a);
        double bb = h(b);
        if (b.piece_index - a.piece_index > 15)
        {
            ff += ((JArray)ExtremeG.pieces).Count;
        }
        else if (a.piece_index - b.piece_index > 15)
        {
            bb += ((JArray)ExtremeG.pieces).Count;
        }
        return ff.CompareTo(bb);
    }
}

public class Model
{
    public static Dictionary<int, double> corner_constants = new Dictionary<int, double>();
    public static Dictionary<int, double> actual_corner_constants = new Dictionary<int, double>();
    public static Dictionary<int, int> ign_map = new Dictionary<int, int>();

    public static bool set_ta = false;
    public static bool set_dr = false;
    public static double throttle_multiplier = 0.2;
    public static double drag = 0.02;

    public static int eqs = 0;
    public static int ign = 0;
    public static double speed_multiplier = -0.3;
    public static double delta_multiplier = -0.01;
    public static double angle_speed_multiplier = -0.00125;
    public static double speed_radius_multiplier = 0.5303;

    public static double corner_force(State st)
    {
        if (!st.is_bend()) return 0;
        double f = speed_multiplier * st.speed + speed_radius_multiplier * st.speed * st.speed / Math.Sqrt(st.radius);
        /*
        if (actual_corner_constants.ContainsKey((int)(st.radius * 1000 + 0.5)))
        {
            f += actual_corner_constants[(int)(st.radius * 1000 + 0.5)] * st.speed * st.speed;
        }
        else
        {
            f += corner_constants[(int)(st.radius * 1000 + 0.5)] * st.speed * st.speed;
            //f += (11.21 * st.radius - 20.995) * st.speed * st.speed;
        }*/
        f = Math.Max(0, f);
        return st.turn_angle >= 0 ? f : -f;
    }

    public static double angle_force(State st)
    {
        return angle_speed_multiplier * st.angle * st.speed + delta_multiplier * st.delta_angle;
    }

    public static double get_dda(State st)
    {
        double from_angles = angle_force(st);
        double corner = corner_force(st);
        return from_angles + corner;
    }

    public static double get_speed(State st)
    {
        return st.speed + st.throttle * throttle_multiplier + drag * st.speed;
    }
}

public class ExtremeG
{
    public static readonly int SEARCH_DEPTH = 20;
    public static readonly int SEARCH_STATES = 600;

    public static State[] all_states = new State[SEARCH_STATES];
    public static State[] next_depth = new State[4 * SEARCH_STATES];
    public static int next_states;
    public static Random rand = new Random(DateTime.Now.Millisecond);

    public static double[,] coefs = new double[4, 5];

    public Bot msg;
    public Logger logger;
    public Model model;

    public int seen = 0;
    public double[] speeds = new double[5];

    public State prev_state;
    public State cur_state;
    public static int maxLane;
    public double next_throttle;
    public static JArray pieces, lanes;
    public string color;

    public static bool haspr = false;
    public static double limit = 55;

    public JObject track
    {
        get { return null; }
        set
        {
            pieces = (JArray)value["pieces"];
            lanes = (JArray)value["lanes"];

            maxLane = 0;
            foreach (JObject lane in lanes)
            {
                maxLane = Math.Max(maxLane, (int)lane["index"]);
            }
        }
    }

    public static double piece_radius(JObject piece, int lane)
    {
        if ((double)piece["angle"] >= 0)
        {
            return (double)piece["radius"] - (double)lanes[lane]["distanceFromCenter"];
        }
        return (double)piece["radius"] + (double)lanes[lane]["distanceFromCenter"];
    }

    public double piece_radius(JObject piece)
    {
        return piece_radius(piece, cur_state.lane);
    }

    public static double piece_length(JObject piece, int lane)
    {
        if (piece["length"] != null) return (double)piece["length"];
        return piece_radius(piece, lane) * Math.Abs(Math.PI * (double)piece["angle"] / 180.0);
    }

    public double piece_length(JObject piece)
    {
        return piece_length(piece, cur_state.lane);
    }

    public ExtremeG(Bot msg)
    {
        this.msg = msg;
        this.logger = new Logger();
        this.model = new Model();
        prev_state = cur_state = new State();
    }

    public static bool sim(State st, double throttle = 1)
    {
        State cur = st.clone();
        //Console.Error.WriteLine("SIMULATE TEH SHIT");
        cur.advance(throttle);
        bool ok = true;

        for (int i = 0; i < 19; i++)
        {
            //if (cur.active != null) Console.Error.WriteLine("have " + cur.active.ticks + " of turbo left", false);
            //cur.debug();
            if (Math.Abs(cur.angle) > limit)
            {
                ok = false;
                break;
            }
            cur.advance(throttle);
        }

        return ok;
    }

    public static bool safetysim(State st, double throttle = 1, bool debug = false)
    {
        State cur = st.clone();
        if (debug) Console.Error.WriteLine("SIMULATE TEH SHIT SAFELY");
        cur.advance(throttle);
        bool ok = true;

        for (int i = 0; i < 200; i++)
        {
            //if (cur.active != null) Console.Error.WriteLine("have " + cur.active.ticks + " of turbo left", false);
            if (debug) cur.debug();
            if (Math.Abs(cur.angle) > limit)
            {
                ok = false;
                break;
            }
            cur.advance(0);
        }

        return ok;
    }

    public static bool ultrasim(State st)
    {
        State cur = st.clone();
        cur.advance(1);
        bool ok = true;

        for (int i = 0; i < 29; i++)
        {
            if (Math.Abs(cur.angle) > limit)
            {
                ok = false;
                break;
            }
            cur.advance(st.turbo.factor);
        }

        return ok;
    }

    public void update_model()
    {
        // ACCELERATION.
        if (!Model.set_ta && cur_state.speed != 0)
        {
            speeds[seen++] = cur_state.speed;
            if (seen == 3)
            {
                double delta1 = speeds[1] - speeds[0];
                double delta2 = speeds[2] - speeds[1];

                Model.drag = (delta2 - delta1) / (speeds[1] - speeds[0]);
                Model.throttle_multiplier = delta1 - speeds[0] * Model.drag;
                logger.log("Setting throttle multiplier to " + Model.throttle_multiplier, true);
                logger.log("Setting drag coefficient to " + Model.drag, true);

                Model.set_ta = true;
                Model.set_dr = true;
            }
        }

        // CURVE PHYSICS.
        if (cur_state.angle != 0)
        {
            if (Model.ign < 2) Model.ign++;
            else if (Model.eqs < 4)
            {
                coefs[Model.eqs, 0] = prev_state.speed * prev_state.angle;
                coefs[Model.eqs, 1] = prev_state.delta_angle;
                coefs[Model.eqs, 2] = prev_state.speed;
                coefs[Model.eqs, 3] = prev_state.speed * prev_state.speed / Math.Sqrt(prev_state.radius);
                coefs[Model.eqs, 4] = cur_state.delta_angle - prev_state.delta_angle;
                Model.eqs++;

                if (Model.eqs == 4)
                {
                    logger.log(LinearEquationSolver.Solve(coefs).ToString(), true);
                    logger.log("setting friction coefficients to ", true);
                    for (int i = 0; i < 4; i++)
                    {
                        logger.log((coefs[i, 0] + " " + coefs[i, 1] + " " + coefs[i, 2] + " " + coefs[i, 3] + " " + coefs[i, 4]), true);
                    }

                    Model.angle_speed_multiplier = coefs[0, 4] / coefs[0, 0];
                    Model.delta_multiplier = coefs[1, 4] / coefs[1, 1];
                    Model.speed_multiplier = coefs[2, 4] / coefs[2, 2];
                    Model.speed_radius_multiplier = coefs[3, 4] / coefs[3, 3];
                }

            }
        }
    }

    public void run(JArray data, int tick)
    {
        JObject my_data = null;
        foreach (JToken some_data in data)
        {
            if ((string)some_data["id"]["color"] == color)
            {
                my_data = (JObject)some_data;
                break;
            }
        }

        JObject my_position = (JObject)my_data["piecePosition"];
        prev_state = cur_state.clone();

        if (cur_state.active != null)
        {
            Console.Error.WriteLine("have " + --cur_state.active.ticks + " of turbo left", true);
            if (cur_state.active.ticks == 0) cur_state.active = null;
        }

        cur_state.lane = (int)my_position["lane"]["endLaneIndex"];
        cur_state.angle = (double)my_data["angle"];
        cur_state.delta_angle = cur_state.angle - prev_state.angle;
        cur_state.piece_index = (int)my_position["pieceIndex"];
        cur_state.inPieceDistance = (double)my_position["inPieceDistance"];
        //cur_state.throttle = next_throttle;

        if (cur_state.piece_index == prev_state.piece_index)
        {
            cur_state.speed = cur_state.inPieceDistance - prev_state.inPieceDistance;
        }
        else
        {
            cur_state.speed = cur_state.inPieceDistance + (piece_length((JObject)pieces[prev_state.piece_index], prev_state.lane) - prev_state.inPieceDistance);
        }

        if (Model.set_dr && Math.Abs(cur_state.speed - prev_state.speed) > 1)
        {
            cur_state.speed = Model.get_speed(prev_state);
        }

        logger.log("piece: " + cur_state.piece_index + " " +
                   "loc = " + cur_state.inPieceDistance + " " +
                   "v=" + cur_state.speed + " " +
                   "angle=" + cur_state.angle, true);

        update_model();


        if (Model.eqs == 4 && maxLane > 0 && cur_state.switch_decision() && rand.NextDouble() > 0.2)
        {
            if (cur_state.lane < maxLane && (cur_state.lane == 0 || rand.NextDouble() > 0.5))
            {
                State newst = prev_state.clone().advance(cur_state.throttle);
                newst.lane++;
                if (safetysim(newst))
                {
                    msg.send(new SwitchLane("Right", tick));
                    return;
                }
            }

            else
            {
                State newst = prev_state.clone().advance(cur_state.throttle);
                newst.lane--;
                if (safetysim(newst))
                {
                    msg.send(new SwitchLane("Left", tick));
                    return;
                }
            }
        }


        if (cur_state.turbo != null && cur_state.active == null && ultrasim(cur_state))
        {
            cur_state.active = cur_state.turbo;
            cur_state.turbo = null;
            msg.send(new TurboMsg(tick));
            return;
        }
        /*
        if (safetysim(cur_state) && sim(cur_state))
        {
            next_throttle = 1;
            msg.send(new Throttle(1));
        }
        else
        {
            next_throttle = 0;
            msg.send(new Throttle(0));
        }*/

        double st = 0, end = 1;
        if (!Model.set_dr)
        {
            msg.send(new Throttle(1, tick));
            return;
        }

        for (int i = 0; i < 20; i++)
        {
            double mid = (st + end) / 2;
            bool debug = i == 19 ? true : false;
            if (safetysim(prev_state, mid, false) && sim(prev_state, mid)) st = mid;
            else end = mid;
        }

        if (st > 0.9999) cur_state.throttle = 0.9999;
        else cur_state.throttle = 0.7 * st;

        logger.log("we're using throttle " + cur_state.throttle + "! Last one was " + prev_state.throttle, false);
        msg.send(new Throttle(cur_state.throttle, tick));

        double delta_delta_angle = cur_state.delta_angle - prev_state.delta_angle;
        logger.log("ST " + delta_delta_angle + " " + Model.get_dda(prev_state), false);
    }

}

public class Bot
{
    public ExtremeG bot;

    public static void Main(string[] args)
    {
        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        // CHANGE ON LAST COMMIT
        //string fn = @"log.txt";
        //TextWriter errStream = new StreamWriter(fn);
        //Console.SetError(errStream);

        Model.corner_constants[40 * 1000] = 0.08385;
        Model.corner_constants[60 * 1000] = 0.068465;
        Model.corner_constants[90 * 1000] = 0.0559;
        Model.corner_constants[110 * 1000] = 0.050565;
        Model.corner_constants[190 * 1000] = 0.0375;
        Model.corner_constants[200 * 1000] = 0.0375;
        Model.corner_constants[210 * 1000] = 0.0375;

        using (TcpClient client = new TcpClient(host, port))
        {
            client.NoDelay = true;
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            // CHANGE ON LAST COMMIT
            new Bot(reader, writer, new Join(botName, botKey));//Race(botName, botKey, "imola"));
            //new JoinRace(botName, botKey, "keimola"));
            //new Join(botName, botKey));
        }

    }

    private StreamWriter writer;

    Bot(StreamReader reader, StreamWriter writer, SendMsg join)
    {
        this.writer = writer;
        string line;
        bot = new ExtremeG(this);

        send(join);

        while ((line = reader.ReadLine()) != null)
        {
            JObject thisJson = JObject.Parse(line);
            try
            {
                switch ((string)thisJson["msgType"])
                {
                    case "carPositions":
                        // CHANGE ON LAST COMMIT
                        if (thisJson["gameTick"] != null)
                        {
                            try
                            {
                                bot.run((JArray)thisJson["data"], (int)thisJson["gameTick"]);
                            }
                            catch
                            {
                                send(new Throttle(0.5, (int)thisJson["gameTick"]));
                            }
                        }
                        break;
                    case "yourCar":
                        bot.color = (string)thisJson["data"]["color"];
                        break;
                    case "join":
                        Console.WriteLine("Joined");
                        //send(new Ping());
                        break;
                    case "gameInit":
                        bot.track = (JObject)thisJson["data"]["race"]["track"];
                        Console.WriteLine("Race init");
                        //send(new Throttle(1));
                        break;
                    case "gameEnd":
                        Console.WriteLine("Race ended");
                        //send(new Ping());
                        break;
                    case "gameStart":
                        Console.WriteLine("Race starts");
                        send(new Throttle(1, (int)thisJson["gameTick"]));
                        break;
                    case "turboAvailable":
                    	if (bot.cur_state.speed > 1e-15) {
                        bot.cur_state.turbo = new Turbo(((int)thisJson["data"]["turboDurationTicks"]) + 1,
                            (double)thisJson["data"]["turboFactor"]);
                    	}
                        break;
                    case "crash":
                        ExtremeG.limit -= 2;
                        bot.cur_state.speed = 0;
                        bot.cur_state.turbo = bot.cur_state.active = null;
                        Model.ign = Model.eqs = 0;
                        break;
                    default:
                        //send(new Ping());
                        break;
                }
            }
            catch
            {
                send(new Ping());
            }
        }
    }

    public void send(SendMsg msg)
    {
        writer.WriteLine(msg.ToJson());
    }
}

class MsgWrapper
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

abstract public class SendMsg
{
    public virtual string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }
    protected virtual Object MsgData()
    {
        return this;
    }

    protected virtual string MsgType()
    {
        return "ping";
    }
}

class Join : SendMsg
{
    public string name;
    public string key;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
    }

    protected override string MsgType()
    {
        return "join";
    }
}

class BotId
{
    public string name;
    public string key;
}

class JoinRace : SendMsg
{
    public BotId botId;
    public string trackName;
    public int carCount;

    public JoinRace(string name, string key, string trackName, int carCount = 1)
    {
        botId = new BotId();
        botId.name = name;
        botId.key = key;
        this.trackName = trackName;
        this.carCount = carCount;
    }

    protected override string MsgType()
    {
        return "joinRace";
    }
}

class Ping : SendMsg
{
    protected override string MsgType()
    {
        return "ping";
    }
}

class Throttle : SendMsg
{
    public string msgType;
    public double data;
    public int gameTick;

    public Throttle(double value, int tick)
    {
        msgType = "throttle";
        this.data = value;
        this.gameTick = tick;
    }

    public override string ToJson()
    {
        return JsonConvert.SerializeObject(this);
    }
}

class TurboMsg : SendMsg
{
    public string msgType;
    public string data;
    public int gameTick;

    public TurboMsg(int tick)
    {
        msgType = "turbo";
        this.data = "";
        this.gameTick = tick;
    }

    public override string ToJson()
    {
        return JsonConvert.SerializeObject(this);
    }
}

class SwitchLane : SendMsg
{
    public string msgType;
    public string data;
    public int gameTick;

    public SwitchLane(string direction, int tick)
    {
        msgType = "switchLane";
        this.data = direction;
        this.gameTick = tick;
    }

    public override string ToJson()
    {
        return JsonConvert.SerializeObject(this);
    }
}

public class Logger
{
    public void log(string msg, bool stdout)
    {
        if (stdout)
        {
            Console.WriteLine(msg);
        }
        Console.Error.WriteLine(msg);
        Console.Error.Flush();
    }
}

public static class LinearEquationSolver
{
    /// <summary>Computes the solution of a linear equation system.</summary>
    /// <param name="M">
    /// The system of linear equations as an augmented matrix[row, col] where (rows + 1 == cols).
    /// It will contain the solution in "row canonical form" if the function returns "true".
    /// </param>
    /// <returns>Returns whether the matrix has a unique solution or not.</returns>
    public static bool Solve(double[,] M)
    {
        // input checks
        int rowCount = M.GetUpperBound(0) + 1;
        if (M == null || M.Length != rowCount * (rowCount + 1))
            throw new ArgumentException("The algorithm must be provided with a (n x n+1) matrix.");
        if (rowCount < 1)
            throw new ArgumentException("The matrix must at least have one row.");

        // pivoting
        for (int col = 0; col + 1 < rowCount; col++) if (M[col, col] == 0)
            // check for zero coefficients
            {
                // find non-zero coefficient
                int swapRow = col + 1;
                for (; swapRow < rowCount; swapRow++) if (M[swapRow, col] != 0) break;

                if (M[swapRow, col] != 0) // found a non-zero coefficient?
                {
                    // yes, then swap it with the above
                    double[] tmp = new double[rowCount + 1];
                    for (int i = 0; i < rowCount + 1; i++)
                    { tmp[i] = M[swapRow, i]; M[swapRow, i] = M[col, i]; M[col, i] = tmp[i]; }
                }
                else return false; // no, then the matrix has no unique solution
            }

        // elimination
        for (int sourceRow = 0; sourceRow + 1 < rowCount; sourceRow++)
        {
            for (int destRow = sourceRow + 1; destRow < rowCount; destRow++)
            {
                double df = M[sourceRow, sourceRow];
                double sf = M[destRow, sourceRow];
                for (int i = 0; i < rowCount + 1; i++)
                    M[destRow, i] = M[destRow, i] * df - M[sourceRow, i] * sf;
            }
        }

        // back-insertion
        for (int row = rowCount - 1; row >= 0; row--)
        {
            double f = M[row, row];
            if (f == 0) return false;

            for (int i = 0; i < rowCount + 1; i++) M[row, i] /= f;
            for (int destRow = 0; destRow < row; destRow++)
            { M[destRow, rowCount] -= M[destRow, row] * M[row, rowCount]; M[destRow, row] = 0; }
        }
        return true;
    }
}